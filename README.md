# SFTP Client

Alpine base image with sftp client and download scripts

## Use

```
docker pull registry.gitlab.com/gabriel-arellano-infra/sftp-client:3.16.0:{tag}
```

## Tags

| Tag    | Alpine | OpenSSH | 
|--------|--------|---------|
| latest | 3.16.0 | 9.0     | 
| 3.16.0 | 3.16.0 | 9.0     | 

