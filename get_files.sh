#!/usr/bin/env bash

cd "$LOCAL_DIR" || exit
sshpass -p "$SSH_PASS" sftp -P "$SSH_PORT" -oHostKeyAlgorithms=+ssh-rsa -oStrictHostKeyChecking=accept-new "$SSH_USER@$SSH_SERVER" << EOF
cd $REMOTE_DIR
get *
quit
EOF