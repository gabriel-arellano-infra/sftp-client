FROM alpine:3.16.0

LABEL maintainer="gabrielarellano@gmail.com"

ENV BASH_VERSION=5.1.16-r2
ENV OPENSSH_VERSION=9.0_p1-r1
ENV SSHPASS_VERSION=1.09-r0

ENV SSH_SERVER=localhost
ENV SSH_PORT=22
ENV SSH_USER=user
ENV SSH_PASS=pass

ENV LOCAL_DIR=/
ENV REMOTE_DIR=.

RUN apk add --no-cache \
    bash=$BASH_VERSION \
    openssh-client=$OPENSSH_VERSION \
    sshpass=$SSHPASS_VERSION

WORKDIR /

COPY get_files.sh /get_files.sh
RUN  chmod a+x /get_files.sh

ENTRYPOINT [ "/get_files.sh" ]
